from flask import Flask,render_template,request,send_file
from flask_sqlalchemy import SQLAlchemy
from send_email import send_email
from sqlalchemy.sql import func
import pandas as pd
from werkzeug import secure_filename
from geopy.geocoders import Nominatim
import numpy as np

app=Flask(__name__)
#app.config['SQLALCHEMY_DATABASE_URI']='postgresql://postgres:XszY-123-pointbreak@localhost/height_collector'

####app.config['SQLALCHEMY_DATABASE_URI']='postgres://cvderpregmwhmp:ca27b31e7b9dd67389aa1c5b63aad5043e777698ccab4ed8898f13b100ca73e7@ec2-23-21-46-94.compute-1.amazonaws.com:5432/dcstgdta2gl5h0?sslmode=require'
####db = SQLAlchemy(app) 
####
####class Data(db.Model):
####	__tablename__ = "data"
####	id=db.Column(db.Integer,primary_key=True)
####	email_=db.Column(db.String(120),unique=True)
####	height_=db.Column(db.Integer)
####
####	def __init__(self,email_,height_):
####		self.email_ = email_
####		self.height_= height_

@app.route("/")
def index():
	return render_template("index.html")

@app.route("/success", methods=['POST'])
def success():
	def return_table(dataframe):
		df=dataframe
		#tablehead=" <!DOCTYPE html> <html> <body> <div> <table border=\"1\">\n "
		#tablefoot=" </table> </div> </body> </html>"
		tablehead=" <table align=\"center\" border=\"1\">\n "
		tablefoot=" </table> "
		firstline = '<tr> '
		for k in range(len(df.columns)):
			firstline = firstline+'<td><strong>'+str(df.columns[k])+'</strong></td>'
		firstline = firstline+'<tr> \n'
		rowline=""
		for j in range(df.shape[0]):
			line='<tr> '
			for k in range(df.shape[1]):
				line=line+"<td>"+str(df.ix[j][k])+"</td>"
			line=line+"</tr>\n"
			rowline=rowline+line
		line = tablehead+firstline+rowline+tablefoot
		return line
	
	def return_new_dataframe(dataframe):
		df = dataframe
		nom = Nominatim()
		new_address_series = df.address+' '+df.City+' '+df.State
		fullgeoseries = new_address_series.apply(nom.geocode)
		latitude = []
		longitude = []
		for items in fullgeoseries:
			try:
				longitude.append(items.longitude)
				latitude.append(items.latitude)
			except:
				longitude.append(np.nan)
				latitude.append(np.nan)

		df['Latitude'] = latitude
		df['Longitude']= longitude
		return df
	
			
	global file
	if request.method=='POST':
		file=request.files["file"]
		file.save(secure_filename("uploaded"+file.filename))
		df = pd.read_csv("uploaded"+file.filename)
		columns_names_lowercase = [item.lower() for item in df.columns]
		if "address" in columns_names_lowercase:
			df = return_new_dataframe(df)
			df.fillna('').to_csv("appended"+file.filename)
			return render_template("index.html",table=return_table(df),btn="download.html")
		else:
			return render_template("index.html",text="It appears that your CSV file does not contain an \"Address\" column.")
		return render_template("index.html",btn="download.html")

@app.route("/download")
def download():
	return send_file("appended"+file.filename,attachment_filename="yourfile.csv",as_attachment=True)


if __name__ == '__main__':
	app.debug=True
	app.run()
